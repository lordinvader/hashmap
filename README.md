# Hash Map
This repository brings an [open addressing](https://en.wikipedia.org/wiki/Open_addressing) hash map implementation in C using double hashing for probing.
The library is made up of three headers (include/hashmap.h, include/hashfun.h, include/compare.h) and three modules (src/hashmap.c, src/hashfun.c, src/compare.c).
You can find documentation right in the source code.

## Features
* Generic types as keys, which are then converted to double using a provided function, look at some implementations in [hashfun.c](src/hashfun.c)
* Generic types as values, compared using a given function, look at some implementations in [compare.c](src/compare.c)
* Auto expands when table is full enought
* [Opaque type](https://en.wikipedia.org/wiki/Opaque_data_type) implementation for information hiding

## Build
To build hashmap as a static library run:
```
>>> make dirs lib
```
then you will find the library at lib/libhashmap.a, you can also use:
```
>>> make
```
which builds the library and some tests.
