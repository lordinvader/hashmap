#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <hashmap.h>
#include <hashfun.h>

#define test(a)\
  fprintf(stderr, "TEST: %50s in function: %50s in file: %20s at line: %5d\t", #a, __func__, __FILE__, __LINE__); \
  if(a) fprintf(stderr, "PASSED\n");                                     \
  else {\
    fprintf(stderr, "NOT PASSED!!\n");\
    exit(-1);\
  }

#define test_many(a)\
  fprintf(stderr, "TEST: %50s in function: %50s in file: %20s at line: %5d\t", #a, __func__, __FILE__, __LINE__); \
  if(a) fprintf(stderr, "PASSED\r");                                     \
  else {\
    fprintf(stderr, "NOT PASSED!!\n");\
    exit(-1);\
  }

static void test_populate(HashMap* hm);
static void test_depopulate(HashMap* hm);
static void test_count_empty();
static void test_update(HashMap* hm);
static void test_foreach(HashMap* hm);

int main(void)
{
  HashMap* hm = HashMap_new(NULL, NULL);

  test_count_empty();
  
  test_populate(hm);
  test_update(hm);
  test_foreach(hm);
  
  test_depopulate(hm);
  
  HashMap_free(hm);
  return 0;
}

static void test_count_empty()
{
  test(HashMap_count(HashMap_new(NULL, NULL))==0);
}

static void test_populate(HashMap* hm)
{
  long n = 1000;
  long i;

  for(i=1; i<n+1; i++)
    HashMap_insert(hm, (void*)i, (void*)i);

  test(HashMap_count(hm)==n);
}

static void test_depopulate(HashMap* hm)
{
  long n = HashMap_count(hm);
  long i;

  for(i=1; i<n+1; i++)
    HashMap_delete(hm, (void*)i);

  test(HashMap_count(hm)==0);
}

static void test_update(HashMap* hm)
{
  long n = HashMap_count(hm);
  long i;
  
  for(i=1; i<n+1; i++) {
    HashMap_update(hm, (void*)i, (void*)(i+1));
    test_many(HashMap_lookup(hm, (void*)i)==(void*)(i+1));
  }
  fprintf(stderr, "\n");
}

static void test_foreach(HashMap* hm)
{
  #ifdef BLOCKS
  HashMap_foreach(hm, ^(void* k, void* v){
      test_many(HashMap_lookup(hm, k)==v);
  });
  fprintf(stderr, "\n");
  #endif
}
