#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <hashmap.h>

#define SAMPLE_SIZE 1000
#define ITERATIONS 50

static void* random_ptr();
static void print_score(double score);

int main()
{
  HashMap* hm = HashMap_new(NULL, NULL);
  void* sample[SAMPLE_SIZE];
  clock_t t1, t2, old_delta = -1;
  int i,j;
  long ocount = 0;
  double score;

  if(clock()==-1) {
    fprintf(stderr, "Can't use CPU clock time. Aborting.\n");
    exit(-1);
  }
    
  for(i=0; i<ITERATIONS; i++) {
    for(j=0; j<SAMPLE_SIZE; j++)
      sample[j] = random_ptr();
    
    t1 = clock();
    for(j=0; j<SAMPLE_SIZE; j++)
      HashMap_insert(hm, sample[j], sample[j]);
    t2 = clock();

    printf("Inserted %10ld items, delta: %5ld", HashMap_count(hm)-ocount, (long)(t2-t1));
    if(old_delta!=-1) {
      score = (double)(t2-t1)/old_delta;
      printf("\t\tfactor: %7.5f", score);
      print_score(t2-t1);
    }
    printf("\n");

    old_delta = t2-t1;
    ocount = HashMap_count(hm);
  }

  HashMap_free(hm);
  return 0;
}

static void* random_ptr()
{
  int i, n = sizeof(void*);
  long res = 0;
  
  for(i=0; i<n; i++)
    res = (res<<8) | (char)rand();
  res *= rand();
  
  return (void*)res;
}

static void print_score(double score)
{
  long i;
  for(i=0; i<5+score/100; i++)
    printf(" ");
  printf("·");
}
